---
title: "Proyectos"
date: 2023-06-29T19:59:59Z
tags:
    - proyectos
---

Hay unos pocos proyectos en los que estoy metido de forma... Bien. Y hay de todo tipo, en progreso, en solitario, de programacion, fuera de programacion,... Me gusta enfangarme en cosas (y, en muchos casos, no terminarlas)

## Tiempo de Cosecha
Creo que es mi ojito derecho. Es la liga de Magic: The Gathering que tengo con mis amigos y esta chulo ver como se puede hacer una aplicacion para llevar las clasificaciones de unos y otros, esta chulo tambien hacer algo con amigos. La mitad de la gente de la liga ha participado en equilibrar los puntos dados, opinar sobre los colorinchis o programar la aplicacion.

Estado: en progreso de una v1.  
No queda practicamente nada para tener una version funcional y bonita. Ya disponemos de una version 0 para poder meter resultados y de todo, pero una v1 es lo que necesitamos para terminar con este proyecto y quitarnos una espinita.

## Pokémon TCG
Fuera de programacion, otra cosa a la que le meto mucho tiempo es al TCG de Pokémon. No solo me cree [una cuenta de Twitter](https://twitter.com/leakspintcg), si no que tambien fui a Turin a participar en un torneo.

Estado: en progreso.  
Creo que esto nunca acabara de completarse, si se completa algun dia, pero es algo de disfrutar por el camino. De momento, tengo muchisimo que aprender, aun sigo cometiendo errores de novato, asi que solo me queda asumir y mejorar.

## Pokémon Collection
A raiz de esto ultimo, intente hacer una pagina para llevar la coleccion de cartas que tengo de Pokémon. El problema? Que no existe programa fiable para moviles para escanear cartas. Y eso es un coñazo

Estado: meh. Me faltaria un sistema para poder crear mazos y guardarlos, pero me da tanta pereza (porque ya existen otras paginas como [pokemoncard.io](https://pokemoncard.io))

## Pokémon TCG PDF Creator
Para torneos presenciales, muchas veces se necesita una hoja con tu mazo relleno. Es un coñazo hacerlo en el pc, en un principio lo hice en Photoshop, pero que para cambiar una linea tenga que abrir el horroroso programa de Adobe, me cansaba mucho.

Esto es una web que, simplemente, tiene un formulario con todos los huecos necesarios para rellenar el PDF, asi como unos botones para exportar en JSON los datos a introducir. Una vez que has hecho el trabajo duro, solo tienes que cargar, modificar lo que quieres, guardar y exportar el PDF.

Estado: completo, no creo que necesite mucho mas para funcionar.

## Lodge
Esto es una aplicacion que hago con un amigo. Este proyecto consiste en llevar un inventario de los productos que tienes comprados, asi como datos utiles (cantidad, caducidad,...) para poder tener, a golpe de movil, que es lo que necesitas o que te caduca dentro de poco. Aqui me encargo de llevar el front, para poder cambiar un poco de aires, asi que a ver que tal se lleva.

Estado: en progreso pero no le estoy haciendo mucho caso. Me tengo que poner las pilas

## Fishing for Gods
Es una pequeña campaña de D&D que estoy haciendo con colegas. Tengo la friolera de dos (2) grupos interesados, asi que va a ser curioso hacer worldbuilding con ambos grupos y ver como coinciden ambas partidas (o no) en este universo.

Mi mayor problema es que, ahora mismo, estoy esperando a que todo este al 100% para empezar y me da que asi no voy a empezar nunca. Me tengo que centrar en que el worldbuilding se hace jugando y que luego tengo que elaborar todo. Pero poco a poco, que tampoco me quiero agobiar.

Estado: en progreso, sin haber empezado. En breves deberia empezar.

## Streaming
Bueno, es un proyecto que tuve hace un tiempo y que lo deje apartado a finales del año pasado por _cosas_. No creo que lo retome por muchas razones, pero una cosa que me... No es molesta, pero me preocupa es que no quiero volver a entrar en las redes sociales del streaming. Quiero saber de las personas, pero me da miedo.

Alejarme me hizo algo de daño psicologicamente, creo, pero tengo que mejorar en ese aspecto. Una asignatura pendiente del 2023.

Estado: parado y sin ganas de volver a retomarlo.

## Majo Sojo
Otra partida de rol, en este caso de chicas magicas. Esta creo que es algo mas elaborada, ya que vamos a ser tres personas las que estamos involucradas en esto y, bueno, hay que darle caña. Lo tengo muy parado, ha sido otro amigo el que ha estado mas a tope al respecto y tambien le tengo que dar.

Estado: boceteado, pero me tengo que poner a ello.

## Paperd.ink
Me pille un display de tinta electronica para poder tener ahi a mano el calendario y la hora, y tiene buena pinta, pero me tengo que poner a programar en C++ y me da mucha pereza ponerme. Asi que con la calma.

Estado: ideado, pero muy abajo en la lista de prioridades.

## Homelab
Yo tengo un homelab aqui en casa desde el cual ofrezco servicios gratuitos a mis amigos para que puedan utilizarlo. Es algo a largo plazo que planeo mantener y aumentar dependiendo de las necesidades que salgan. Nextcloud es una pasada y tener una VPN para conectarme desde fuera a mi casa, lo mejor de la vida.

Estado: muy estable y muy contento.

## Impresion 3D
Tambien tengo una impresora 3d para imprimir cositas. Como son pocas cosas las que quiero hacer (jeje), he decidido que no voy a aprender 3D para poder imprimirme mis cosas. Si, se que lo mejor de imprimir 3d es diseñarte tu las cosas, asi que intentare commisionear a amigos para que me hagan ese trabajo que no se hacer y que no tengo tiempo para aprender.

Estado: imprimiendo, que imprime muy bien y ya me ha salvado de alguna.

## Watchy
Como me flipan las pantallas de tinta electronica, tambien me pille un reloj de tinta electronica. Ahora mismo, el reloj es funcional y me mola muchisimo. El unico problema es que me molaria hacer un tamagotchi en el propio reloj, pero implica un poco lo mismo que he dicho en Paperd.ink y, claro, pues dificil.

Estado: funcional, en espera a ver que narices hago.

## Gatos
Es un proyecto a futuro que quiero que se haga realidad en la segunda quincena de agosto. Se necesita hacer porque aqui estoy solo y creo que me vendria bien la compañia. Y es algo que me va a servir para alejarme del pc de vez en cuando.

Estado: con fecha de lanzamiento.

---

Y creo que ya. Este listado era, tambien, para tener un poco en cuenta que es lo que tengo entre manos. Plasmarlo yo aqui hace que me de cuenta del tiempo que tengo y de lo que quiero priorizar. Quiero esto? Quiero lo otro? Quiero terminar esto ya y asi me lo quito?

Asi que eso, me gusta mucho hacer cositas.