---
title: "(Neo/Geo)cities"
date: 2023-07-03T17:29:43Z
draft: true
---

Pues, como no tengo ningun proyecto de programacion asi y quiero tener algo que me produzca gustito en el kokoro, voy a intentar hacer una web al estilo geocities, inspirado un poco en [hotline webring](https://hotlinewebring.club/).

Uno de sus miembros, melankorin tiene [una entrada](https://melankorin.net/blog/2023/06/19/) en la que explica como planear este tipo de webs, asi que voy a comentar por aqui un poco el diario de desarrollo y las decisiones que tomo. Tambien esto me viene bien para guardar cosas y decisiones antes de commitear nada, que tampoco hace falta crear un repo para todo (que esta bien, pero si no es mucho agobio).

Colores predominantes: blanco, negro y morado.  
Tipografia: monotipo, probablemente [Inconsolata](https://fonts.google.com/specimen/Inconsolata).  
Imagenes: a poder ser, delineados, nada de color.

## Estructura

1. Home
    1. Pequeña introduccion
    1. 3 ultimas entradas del blog
    1. Algunas banderas
1. CV
    1. Pasar directamente el CV que tengo en mi web oficial a esta
    1. Asegurarme de que es una version compatible con impresion/PDF
1. Blog
    1. Blog normalito, sin nada raro
    1. RSS

## Bocetos de diseño

### Ideas de patrones para el fondo
```css
body {
    background-color: #26292b;
    background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='28' height='49' viewBox='0 0 28 49'%3E%3Cg fill-rule='evenodd'%3E%3Cg id='hexagons' fill='%236c6080' fill-opacity='0.25' fill-rule='nonzero'%3E%3Cpath d='M13.99 9.25l13 7.5v15l-13 7.5L1 31.75v-15l12.99-7.5zM3 17.9v12.7l10.99 6.34 11-6.35V17.9l-11-6.34L3 17.9zM0 15l12.98-7.5V0h-2v6.35L0 12.69v2.3zm0 18.5L12.98 41v8h-2v-6.85L0 35.81v-2.3zM15 0v7.5L27.99 15H28v-2.31h-.01L17 6.35V0h-2zm0 49v-8l12.99-7.5H28v2.31h-.01L17 42.15V49h-2z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E");
}
```

![Patron hexagonal para el fondo](/images/geocities-body-pattern.png)

### Arquitectura?
Supongo que podre reusar hugo y darle forma a lo que quiero. Y, por ello, tendre que hacerme un tema personalizado :D

![Primer boceto, muy parecido al de melankorin](/images/geocities-first-draft.png)