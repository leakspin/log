---
title: "Resque Debug"
date: 2023-06-30T10:23:17Z
tags:
    - trabajo
    - programacion
draft: true
---

Bueno, pues primer post sobre trabajo.

Llevo una semana debuggeando un proceso que tenemos en mi empresa para procesar trabajos de colas. Por diferentes motivos, utilizamos una libreria vieja llamada [php-resque](https://github.com/resque/php-resque). Es... viejita. Y, ademas, tenemos hecho un fork, asi que tambien es dificil.

## El Problema
Basicamente, estamos en medio de una actualizacion de imagen base (de 18.04 a 22.04), actualizacion de PHP (de 7.4 a 8.2) y actualizacion de algunas librerias. Pues parece que eso a resque no le mola, pero no de la manera que estas creyendo.

Al ser colas, tenemos automatizado en demonios la ejecucion de dichas colas. Por lo que delegamos en systemd que se ejecuten las colas y listo.

Pues el problema es que, ejecutando el proceso de colas a mano funciona pero mediante systemd no.

### Systemd bajo lupa
Aqui empezamos a investigar que narices pasaba con systemd y que diferencias podria haber con una shell. Pero no habia nada raro. Despues de logs, cambios en la forma de ejecutarlo desde la shell para asimilar a systemd,... No encontramos nada

Y, entonces, empece a investigar htop y vi que el estado de la mayor parte de los procesos estaba en `D` (Uninterruptible sleep). Estaban todos esperando I/O? Bueno, normal, no? Hace un sleep para checkear la cola y, entonces, ejecutar el job. Asi que eso no deberia ser... Pero habra pasado algo con el proceso?

### La magia de /proc
Mira que he estado administrando sistemas Linux y desplegando cosas, pero nunca me habia puesto a debuggear el como se ejecutaba un proceso en Linux y como se podia investigar. Resulta que `/proc` tiene la magia al respecto de investigar procesos, y me puse a ello.

En este caso, me centre mas en `syscall` y `stack`, para intentar adivinar que narices pasa.

Y en syscall, si que me he encontrado esto en muchos de esos procesos:
```
-1 0x7fff031e2338 0x7f7f76fdba70
```

Alarmas, el `-1` es un error. Pero por que? Si no aparece nada en el journal del proceso...

Una idea! Cambiar los fwrite a echos para evitar escribir de aquella manera a los file descriptors!

No funciona......