---
title: "The Legend of Zelda: Tears of the Kingdom"
date: 2023-06-25T19:58:37Z
tags:
    - juegos
---

Pues llevo como +50h a dia de hoy y no me esta gustando tanto como BotW. Es dificil hacer un juego mejor que BotW, eso esta claro, pero es que TotK es _El mismo juego_ sin el 2. La historia es simplistamente igual y no es que innove en ningun lado.

Se ve progresion de los personajes, la historia ha cambiado y continua: nuevos personajes, personajes viejos con nuevas historias y el mundo ha cambiado. Pero no, el mundo no ha cambiado nada: todo sigue en su mismo lugar. Y da un poco de pena porque las historias son practicamente iguales pero con pequeños matices. Son tan pequeños que me da pereza seguir.

No se si sera la desidia del momento de mi vida, pero ahora mismo me llama mas leer alguna otra historia que seguir jugando a este juego. Y la dinamica del aire me flipa, es de lo que mas. La del subsuelo tambien si no me diera tanto miedo, pero esta super chulo.

Entonceeeeeeeeeeeeeeeees pues no se. Aqui estoy, divagando mientras. Mismo jugar a Coffee Talk 2 o ANNO: Mutationem me haga ver la luz.

